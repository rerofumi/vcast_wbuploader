FROM debian

RUN apt-get update; \
    apt-get -y upgrade; \
    apt-get -y install ruby ruby-dev imagemagick; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*; \
	gem install --no-doc sinatra

WORKDIR /root

ADD pdf-uploader/image_public.rb /root/image_public.rb
ADD pdf-uploader/uploader.rb /root/uploader.rb
ADD pdf-uploader/sub_convert.rb /root/sub_convert.rb
ADD start.sh /root/start.sh
RUN chmod 755 /root/start.sh

EXPOSE 8080 4567
CMD ["./start.sh"]
