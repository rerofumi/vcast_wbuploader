require 'fileutils'
require 'digest/sha1'

def dirinit
  begin
    Dir.mkdir('workspace', 0777)
  rescue => e
  end
  begin
    Dir.mkdir('workspace/page', 0777)
  rescue => e
  end
  begin
    Dir.mkdir('image', 0777)
  rescue => e
  end
end

def clean
  FileUtils.rm(Dir.glob('./workspace/page/*.jpg'))
  FileUtils.rm(Dir.glob('./workspace/*.jpg'))
end

def convert(filename)
  system "convert #{filename} ./workspace/page/page-%03d.jpg"
  result = store_image(cnv_hash(filename))
  clean()
  result
end

def listup
  Dir.glob('./workspace/page/page-*').sort
end

def cnv_hash(filename)
  list = []
  org = listup()
  org.each do |item|
    seed = filename + item
    hash = Digest::SHA1.hexdigest(seed)
    newpath = File.dirname(item) + '/' + hash + '.jpg'
    FileUtils.copy(item, newpath)
    list << newpath
  end
  list
end

def store_image(list)
  whiteboard = []
  list.each do |item|
    fn = File.basename(item)
    FileUtils.copy(item, './image/'+fn)
    whiteboard << URLBASE+fn
  end
  { whiteboard: { urls: whiteboard }}
end

