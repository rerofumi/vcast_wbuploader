require 'sinatra'
require 'json'

$:.unshift File.dirname(__FILE__)
require 'sub_convert.rb'

$stdout.sync = true

if ENV["URLBASE"].nil?
  URLBASE='http://localhost:8080/'
else
  URLBASE=ENV['URLBASE']
end
puts "URL: #{URLBASE}"

dirinit()

post '/convert' do
  r = {}
  if params[:file]
    filename = "./workspace/#{params[:file][:filename]}"
    File.open(filename, "wb") do |fp|
      fp.write params[:file][:tempfile].read
      # process
      begin
         r = convert(filename)
      rescue => e
        puts e
        r[:reason] = "comvert error"
        r[:result] = false
      end
    end
  else
    r[:result] = false
  end
  r.to_json
end

